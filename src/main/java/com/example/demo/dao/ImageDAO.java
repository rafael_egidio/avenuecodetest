package com.example.demo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.example.demo.domain.Image;
import com.example.demo.domain.Product;

/**
 * @author rafael
 *
 */
@RestResource(path="images", rel="images" )
public interface ImageDAO extends CrudRepository<Image, Long> {
	
	
	List<Image>findAll();
	
	Image findByName(String name);
	
	List<Image> findByPosition(String position);
	
	List<Image> findByTeam(Product team);
	
	List<Image> findByTeamId(Long team);
	List<Image> findByTeamName(String teamName);
	
	
	
}
