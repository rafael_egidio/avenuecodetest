package com.example.demo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RestResource;

import com.example.demo.domain.Product;

/**
 * @author rafael
 *
 */
@RestResource(path="products", rel="products" )
public interface ProductDAO extends CrudRepository<Product, Long> {
	
	
	List<Product>findAll();
	
	Product findByName(String name);
	
	List<Product> findByPlayersName(String name);
	

}
