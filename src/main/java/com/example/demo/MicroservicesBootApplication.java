package com.example.demo;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import com.example.demo.dao.ProductDAO;
import com.example.demo.domain.Image;
import com.example.demo.domain.Product;

/**
 * @author rafael
 *
 */
@SpringBootApplication
public class MicroservicesBootApplication extends SpringBootServletInitializer{

	/**
	 * Used when run as a JAR
	 */
	public static void main(String[] args) {
		SpringApplication.run(MicroservicesBootApplication.class, args);
	}

//	/**
//	 * Used when run as a WAR
//	 */
//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
//		return builder.sources(MicroservicesBootApplication.class);
//	}
	
//	@PostConstruct
//	public void init() {
//		Set<Image> players = new HashSet<>();
//		Image rafael = new Image("Rafael","Volante");
//		players.add(rafael);
//		players.add(new Image("Egidio", "Zagueiro"));
//		
//		Product team = new Product("500","Contagem","Mula",players);
//		
//		teamDao.save(team);
//		
//		players.clear();
//		players.add(new Image("Robinho","Volante"));
//		players.add(new Image("Arrascaeta","Meia"));
//		players.add(new Image("Fred","Atacante"));
//		players.add(new Image("Thiago Neves","Meia"));
//		players.add(new Image("Sassa","Atacante"));
//		players.add(new Image("Rafael","Goleiro"));
//		
//		Product team2 = new Product("Cruzeiro","Belo Horizonte","Raposa",players);
//		
//		
//		
//		teamDao.save(team2);
//	}
	
}
