package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.ImageDAO;
import com.example.demo.dao.ProductDAO;
import com.example.demo.domain.Image;
import com.example.demo.domain.Product;

/**
 * @author rafael
 *
 */
@RestController
public class Controller {
	@Autowired
	ProductDAO teamDao;
	
	@Autowired
	ImageDAO playerDao;

	
	@RequestMapping(value="/playersByTeam/", method=RequestMethod.POST)
	public  List<Image> playersByTeam(@RequestBody Product team){
		return playerDao.findByTeam(team);
	}
//	@RequestMapping(value="/teamsByPlayer/", method=RequestMethod.POST)
//	public  List<Team> teamsByPlayer(@RequestBody Player player){
//		return teamDao.findByPlayers(player);
//	}
	@RequestMapping("/playersByTeam/{id}")
	public  List<Image> playersByTeamId(@PathVariable Long id){
		return playerDao.findByTeamId(id);
	}
	@RequestMapping("/playersByTeamName/{teamName}")
	public  List<Image> playersByTeamId(@PathVariable String teamName){
		return playerDao.findByTeamName(teamName);
	}
	@RequestMapping("/teamsByPlayer/{name}")
	public  List<Product> hiThere(@PathVariable String name){
		return teamDao.findByPlayersName(name);
	}
	@RequestMapping("/playerByPosition/{position}")
	public  List<Image> findByPostion(@PathVariable String position){
		return playerDao.findByPosition(position);
	}
	
	
	
	

}
