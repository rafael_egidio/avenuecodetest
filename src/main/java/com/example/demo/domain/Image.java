package com.example.demo.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * 
 * @author rafael
 *
 */
@Entity
public class Image {
	
	@Id
	@GeneratedValue
	Long id;
	String name;
	String position;
	
	@ManyToOne(fetch = FetchType.LAZY)
	Product team;
	
	
	public Image() {
		super();
	}
	public Image(String name, String position) {
		super();
		this.name = name;
		this.position = position;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Product getTeam() {
		return team;
	}
	public void setTeam(Product team) {
		this.team = team;
	}

}
